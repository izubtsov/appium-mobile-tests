const { exec } = require('child_process');
const { argv } = require('yargs');

const application = argv.app;

const PERMISSIONS = {
    CAMERA: 'android.permission.CAMERA',
    FILES: 'android.permission.WRITE_EXTERNAL_STORAGE',
    LOCATION: 'android.permission.ACCESS_FINE_LOCATION',
    MICROPHONE: 'android.permission.RECORD_AUDIO',
};

/**
 * Предоставляет приложению разрешения к переданным функциям
 * @param permissionArray - массив функций, к которым необходимо предоставить доступ приложению
 */
function grantPermissions (permissionArray) {
    permissionArray.forEach((item) => {
        exec(`adb shell pm grant ${application} ${PERMISSIONS[item]}`);
    });
}

export default grantPermissions;
