const buttons = {
    switchModeButton: '~more',
    returnButton: '~Return',
    shiftButton: '~shift',
    doneButton: '~Done',
    searchButton: '~Search',
};

/**
 * Ввод с клавиатуры переданного текста
 * @param text - массив функций, к которым необходимо предоставить доступ приложению
 */
function enterByKeyboard (text) {
    text.split('').forEach((symbol) => {
        if (!isLetter(symbol)) {
            tapSwitch();
            $(`//*[@label='${symbol}']`).click();
            tapSwitch();
        } else if (checkCase(symbol)) {
            tapShift();
            $(`//*[@label='${symbol}']`).click();
        } else {
            $(`//*[@label='${symbol}']`).click();
        }
    });
}

/**
 * Проверяет, является переданный символ буквой
 * @param symbol - символ
 * @returns {*}
 */
function isLetter (symbol) {
    return /[A-z]/.test(symbol);
}

/**
 * Переключение режима ввода
 */
function tapSwitch () {
    $(buttons.switchModeButton).click();
}

/**
 * Проверяет регистр переданной буквы
 */
function checkCase (symbol) {
    return symbol === symbol.toUpperCase();
}

/**
 * Переключение режима ввода
 */
function tapShift () {
    $(buttons.shiftButton).click();
}

module.exports = {
    buttons,
    enterByKeyboard
};
