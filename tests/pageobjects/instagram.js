const SELECTORS = {
    LOGIN_BUTTON: 'android=new UiSelector().resourceId("com.instagram.android:id/log_in_button")',
    TEXT_FIELDS_COLLECTION: 'android.widget.EditText',
    LOGIN_WITH_ACCOUNT_BUTTON: 'android.widget.Button',
    ADD_POST_BUTTON: 'android=new UiSelector().resourceId("com.instagram.android:id/creation_tab")',
    CAMERA_BUTTON: 'android=new UiSelector().resourceId("com.instagram.android:id/unified_camera_button")',
    TAKE_A_PHOTO_BUTTON: 'android=new UiSelector().resourceId("com.instagram.android:id/camera_shutter_button")',
    NEXT_BUTTON: 'android=new UiSelector().resourceId("com.instagram.android:id/next_button_imageview")',
    PHOTO_CAPTION: 'android=new UiSelector().resourceId("com.instagram.android:id/caption_text_view")',
    POST_DESCRIPTION: 'android=new UiSelector().resourceId("com.instagram.android:id/row_feed_comment_textview_layout")',
};

class Instagram {
    get loginButton () {
        return $(SELECTORS.LOGIN_BUTTON);
    }

    get textFieldsCollection () {
        return $$(SELECTORS.TEXT_FIELDS_COLLECTION);
    }

    get loginWithAccountButton () {
        return $$(SELECTORS.LOGIN_WITH_ACCOUNT_BUTTON);
    }

    get addPostButton () {
        return $(SELECTORS.ADD_POST_BUTTON);
    }

    get cameraButton () {
        return $(SELECTORS.CAMERA_BUTTON);
    }

    get takeAPhotoButton () {
        return $(SELECTORS.TAKE_A_PHOTO_BUTTON);
    }

    get nextButton () {
        return $(SELECTORS.NEXT_BUTTON);
    }

    get photoCaption () {
        return $(SELECTORS.PHOTO_CAPTION);
    }

    get postDescription () {
        return $(SELECTORS.POST_DESCRIPTION);
    }
}

export default new Instagram();
