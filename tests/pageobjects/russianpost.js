const SELECTORS = {
    ACTION_OTHER_BUTTON: '~rPTabBar_moreTabBarItem',
    REGISTRATION_AND_LOGIN_BUTTON: '~moreStaticCell_authorization',
    SEARCH_TRACK_INPUT: '~trackingItemList_searchBar',
    TRACKING_STATUS: '~CurrentTrackingItemStatus',
    MORE_ADVANTAGES: '~moreStaticCell_advertising',
};

class Russianpost {
    get actionOtherButton () {
        return $(SELECTORS.ACTION_OTHER_BUTTON);
    }

    get registrationAndLoginButton () {
        return $(SELECTORS.REGISTRATION_AND_LOGIN_BUTTON);
    }

    get searchTrackInput () {
        return $(SELECTORS.SEARCH_TRACK_INPUT);
    }

    get trackingStatus () {
        return $(SELECTORS.TRACKING_STATUS);
    }

    get moreAdvantages () {
        return $(SELECTORS.MORE_ADVANTAGES);
    }
}

export default new Russianpost();
