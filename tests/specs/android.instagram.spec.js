import instagramConfig from '../../config/instagram/instagram.config';
import Instagram from '../pageobjects/instagram';
import grantPermissions from '../helpers/Permissions';
const { exec } = require('child_process');
const { argv } = require('yargs');

const { readFileSync } = require('fs');
const path = require('path');

const application = argv.app;
const postDescriptionText = 'Тестовый тест';

describe('Тестирование приложения Instagram', () => {
    beforeAll(() => {
        // установка приложения на смартфон, если оно еще не установлено
        if (!driver.isAppInstalled(application)) {
            driver.installApp(path.normalize(path.resolve('./apps/instagram.apk')));
        }
        // предоставление приложению необходимых разрешений
        grantPermissions(['CAMERA', 'FILES', 'LOCATION', 'MICROPHONE']);
    });
    beforeEach(() => {
        // запуск приложения
        driver.activateApp(application);
        // ожидание загрузки приложения
        driver.waitUntil(() => driver.queryAppState(application) === 4 && driver.getCurrentActivity() === '.activity.MainTabActivity');
        // авторизация в приложении, если еще не авторизованы
        if (Instagram.loginButton.isDisplayed()) {
            Instagram.loginButton.click();
            Instagram.textFieldsCollection[0].setValue(instagramConfig.login);
            Instagram.textFieldsCollection[1].setValue(instagramConfig.password);
            Instagram.loginWithAccountButton[1].click();
        }
    });

    it('добавление поста с использованием камеры', () => {
        // нажатие на кнопку добавление поста
        Instagram.addPostButton.waitForDisplayed();
        Instagram.addPostButton.click();
        // нажатие на кнопку камеры
        Instagram.cameraButton.click();
        // нажатие на кнопку "сделать фото"
        Instagram.takeAPhotoButton.click();
        Instagram.nextButton.waitForDisplayed();
        Instagram.nextButton.click();
        // добавление описания посту
        Instagram.photoCaption.setValue(postDescriptionText);
        Instagram.nextButton.click();
        // проверка, что пост добавился
        Instagram.postDescription.waitForDisplayed();
        expect(Instagram.postDescription.getText()).toContain(postDescriptionText);
    });

    it('добавление поста c использованием хранилища фотографий', () => {
        // добавление на устройство фотографии
        const file = readFileSync(path.normalize(path.resolve('./files/test.jpg')), 'base64');
        driver.pushFile('/storage/emulated/0/Pictures/test.jpg', file);
        // нажатие на кнопку добавление поста
        Instagram.addPostButton.waitForDisplayed();
        Instagram.addPostButton.click();

        Instagram.nextButton.click();
        Instagram.nextButton.click();
        // добавление описания посту
        Instagram.photoCaption.setValue(postDescriptionText);
        Instagram.nextButton.click();
        // проверка, что пост добавился
        Instagram.postDescription.waitForDisplayed();
        expect(Instagram.postDescription.getText()).toContain(postDescriptionText);
    });

    afterEach(() => {
        // закрытие приложения после каждого теста
        driver.terminateApp(application);
    });

    afterAll(() => {
        // закрытие приложения после каждого теста
        driver.removeApp(application);
        // удаление загруженного на девайс файла
        exec('adb shell rm \'/storage/emulated/0/Pictures/test.jpg\'');
    });
});
