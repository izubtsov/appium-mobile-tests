import russianPostConfig from '../../config/russianpost/russianpost.config';
import Russianpost from '../pageobjects/russianpost';
import { enterByKeyboard, buttons } from '../helpers/Keyboard';
const { argv } = require('yargs');

const application = argv.app;

describe('Тестирование приложения почта России', () => {
    beforeEach(() => {
        // запуск приложения
        driver.activateApp(application);
    });

    it('авторизация в приложении и проверка работы поиска', () => {
        // авторизация в приложении
        Russianpost.actionOtherButton.click();
        Russianpost.registrationAndLoginButton.click();
        // ввод логина
        enterByKeyboard(russianPostConfig.login);
        $(buttons.returnButton).click();
        // ввод пароля
        enterByKeyboard(russianPostConfig.password);
        $(buttons.doneButton).click();
        // поиск отправления
        Russianpost.searchTrackInput.setValue(russianPostConfig.trackNumber);
        $(buttons.searchButton).click();
        Russianpost.trackingStatus.waitForDisplayed();
    });

    afterEach(() => {
        // закрытие приложения после каждого теста
        driver.terminateApp(application);
    });
});
