# appium-mobile-tests

Особенности запуска на устройствах с ОС Android:
- перед запуском тестов для корректной работы интернета на устройстве необходимо запустить эмулятор через терминал командой 
`emulator.exe -avd <device_name> -dns-server 8.8.8.8`
- конфигурация запуска - `wdio ./config/wdio.android.app.conf.js "--app="<app_id>" "--avd=<device_name>"`, здесь app_id - имя пакета приложения (com.instagram.android), device_name - имя эмулируемого девайса.
- в файле config/instagram/instagram.config.js указать логин и пароль от учетной записи Instagram.

Особенности запуска на устройствах с ОС iOS:
- конфигурация запуска - `wdio ./config/wdio.ios.app.conf.js "--app=<bundle_id>" "--deviceName=<device_name>" "--udid=<udid>"`, здесь bundle_id - имя пакета приложения (ru.russianpost.Russian-Post), device_name - имя подключенного iOS-устройства, udid - уникальный идентификатор подключенного iOS-устройства.
- в файле config/russianpost/russianpost.config.js указать логин и пароль от учетной записи Почты России.

