const { argv } = require('yargs');
const { config } = require('./wdio.shared.conf');

// ============
// Specs
// ============
config.specs = [
    './tests/specs/**/android*.spec.js',
];

// ============
// Capabilities
// ============
config.capabilities = [
    {
        platformName: 'Android',
        maxInstances: 1,
        'appium:deviceName': 'Android Emulator',
        'appium:platformVersion': '11',
        'appium:orientation': 'PORTRAIT',
        'appium:automationName': 'UiAutomator2',
        'appium:noReset': true,
        'appium:newCommandTimeout': 240,
        'appium:avd': (argv.avd !== undefined) ? argv.avd : 'Pixel_3a_API_30_x86',
    },
];

exports.config = config;
