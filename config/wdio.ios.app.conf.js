const { argv } = require('yargs');
const { config } = require('./wdio.shared.conf');

// ============
// Specs
// ============
config.specs = [
    './tests/specs/**/ios*.spec.js',
];

// ============
// Capabilities
// ============
config.capabilities = [
    {
        platformName: 'iOS',
        'appium:deviceName': argv.deviceName,
        'appium:orientation': 'PORTRAIT',
        udid: argv.idid,
        'appium:automationName': 'XCUITest',
        'appium:noReset': true,
        'appium:newCommandTimeout': 240,
    },
];

exports.config = config;
